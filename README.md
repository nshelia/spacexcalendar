# SpaceX Calendar :rocket:

Get information about SpaceX upcoming missions, and achivements. :tada: 

### [Website](https://spacexcalendar-v2.herokuapp.com/)

### Some nice features include

* SSR Support
* Code-splitting using [React Loadable](https://github.com/jamiebuilds/react-loadable)
* Styled Components v4

### Tech Stack 

* React
* Redux
* React Router
* Styled Components
* Babel
* Webpack v4
* Express
* [react-frontload](https://github.com/davnicwil/react-frontload)
* Heroku